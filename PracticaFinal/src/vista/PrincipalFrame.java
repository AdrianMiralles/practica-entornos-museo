package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;
import javax.swing.JTable;
import java.awt.CardLayout;
import javax.swing.JMenuBar;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JToolBar;
import javax.swing.JMenu;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class PrincipalFrame extends JFrame {

	private JPanel contentPane;
	private JMenuBar menuBar;
	private JPanel pCrear;
	private JPanel pModificar;
	private JToolBar toolBar;
	private JButton buscador;
	private JMenu mnMenu;
	private JMenu mnMantMenu;
	private JButton btnCrear;
	private JButton btnModificar;
	private JButton btnBorrar;
	private JMenu mnConsultaMenu;
	private JButton btnConsultarButton;
	private JPanel pBorrar;
	private JPanel pConsultar;
	private JTextField tfIdentificador;
	private JTextField tfNombre;
	private JTextField tfNumJugadores;
	private JTextField tfFechaVer;
	private JTextField tfDesarrolladora;
	private JTextField tfEditora;
	private JTextField tfGenero;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PrincipalFrame frame = new PrincipalFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PrincipalFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 541, 482);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnMenu = new JMenu("Menu");
		menuBar.add(mnMenu);
		
		mnMantMenu = new JMenu("Mantenimiento");
		mnMenu.add(mnMantMenu);
		
		btnCrear = new JButton("Crear");
		mnMantMenu.add(btnCrear);
		
		btnModificar = new JButton("Modificar");
		mnMantMenu.add(btnModificar);
		
		btnBorrar = new JButton("Borrar");
		mnMantMenu.add(btnBorrar);
		
		mnConsultaMenu = new JMenu("Consulta");
		mnMenu.add(mnConsultaMenu);
		
		btnConsultarButton = new JButton("Consultar");
		btnConsultarButton.setForeground(Color.BLACK);
		btnConsultarButton.setBackground(Color.WHITE);
		mnConsultaMenu.add(btnConsultarButton);
		
		toolBar = new JToolBar();
		menuBar.add(toolBar);
		
		buscador = new JButton("Buscar");
		toolBar.add(buscador);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		
		pCrear = new JPanel();
		pCrear.setBackground(UIManager.getColor("Panel.background"));
		contentPane.add(pCrear, "t1");
		pCrear.setLayout(null);
		
		JButton btnAniadir = new JButton("A\u00F1adir");
		btnAniadir.setBounds(426, 385, 89, 23);
		pCrear.add(btnAniadir);
		
		JLabel lblNewLabel = new JLabel("Identificador:");
		lblNewLabel.setBounds(10, 28, 101, 14);
		pCrear.add(lblNewLabel);
		
		tfIdentificador = new JTextField();
		tfIdentificador.setBounds(121, 25, 131, 20);
		pCrear.add(tfIdentificador);
		tfIdentificador.setColumns(10);
		
		tfNombre = new JTextField();
		tfNombre.setBounds(343, 25, 146, 20);
		pCrear.add(tfNombre);
		tfNombre.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre:");
		lblNewLabel_1.setBounds(262, 28, 46, 14);
		pCrear.add(lblNewLabel_1);
		
		tfNumJugadores = new JTextField();
		tfNumJugadores.setBounds(121, 74, 131, 20);
		pCrear.add(tfNumJugadores);
		tfNumJugadores.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("N\u00FAmero jugadores:");
		lblNewLabel_2.setBounds(10, 77, 101, 14);
		pCrear.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Fecha version:");
		lblNewLabel_3.setBounds(262, 77, 71, 14);
		pCrear.add(lblNewLabel_3);
		
		tfFechaVer = new JTextField();
		tfFechaVer.setBounds(343, 74, 146, 20);
		pCrear.add(tfFechaVer);
		tfFechaVer.setColumns(10);
		
		tfDesarrolladora = new JTextField();
		tfDesarrolladora.setBounds(121, 123, 131, 20);
		pCrear.add(tfDesarrolladora);
		tfDesarrolladora.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Desarrolladora:");
		lblNewLabel_4.setBounds(10, 126, 101, 14);
		pCrear.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Editora:");
		lblNewLabel_5.setBounds(262, 126, 46, 14);
		pCrear.add(lblNewLabel_5);
		
		tfEditora = new JTextField();
		tfEditora.setBounds(343, 123, 146, 20);
		pCrear.add(tfEditora);
		tfEditora.setColumns(10);
		
		tfGenero = new JTextField();
		tfGenero.setBounds(121, 172, 131, 20);
		pCrear.add(tfGenero);
		tfGenero.setColumns(10);
		
		JLabel lblNewLabel_6 = new JLabel("Genero:");
		lblNewLabel_6.setBounds(10, 175, 46, 14);
		pCrear.add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("Clasi. Edades:");
		lblNewLabel_7.setBounds(262, 175, 71, 14);
		pCrear.add(lblNewLabel_7);
		
		textField = new JTextField();
		textField.setBounds(343, 172, 146, 20);
		pCrear.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(121, 221, 131, 20);
		pCrear.add(textField_1);
		textField_1.setColumns(10);
		
		pModificar = new JPanel();
		contentPane.add(pModificar, "t2");
		pModificar.setLayout(null);
		
		pBorrar = new JPanel();
		contentPane.add(pBorrar, "name_968593419975600");
		pBorrar.setLayout(null);
		
		pConsultar = new JPanel();
		contentPane.add(pConsultar, "name_968743030280300");
		pConsultar.setLayout(null);
	}
}
